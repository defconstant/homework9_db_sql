SELECT student.`group`, marks.subject, AVG(marks.mark)
FROM marks JOIN student ON
marks.student_id = student.student_id
WHERE marks.subject = 'Java'
GROUP BY student.`group`;

SELECT student.`name`, COUNT(marks.mark)
FROM marks JOIN student ON
student.student_id = marks.student_id
GROUP BY student.`name`;
